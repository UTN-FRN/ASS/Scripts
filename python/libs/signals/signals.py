class signals:

    def __init__(self):
        self.s = lambda t: (t >= 0) and 1 or 0
        self.i = lambda t: (t == 0) and 1 or 0
        self.r = lambda t: (t >= 0) and t or 0

    def step(self, t):
        return self.s(t)

    def impulse(self, t):
        return self.i(t)

    def ramp(self, t):
        return self.r(t)
